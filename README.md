# Setup

Create virtualenv and install maturin (i had to deactivate all pyenv 3.x versions except one, so that maturin works)
```
python3.6 -m venv .venv
.venv/bin/pip install maturin
```

Run maturin, which builds the rust cffi python lib and creates a wheel for it and then install the wheel:
```
.venv/bin/maturin build
.venv/bin/pip install --upgrade target/wheels/delayer-0.1.0-cp36-cp36m-macosx_10_7_x86_64.whl
```
# Running
Run the python script in dela via:
```
.venv/bin/python3 dela/__init__.py
```

The script is simple:
- it adds two numbers and returns those as a string
- the first number represents the max timeout in seconds
- the second number represents the processing time of the async function
- this means: only for b>a, the function returns a value, otherwise it returns a timeout error, thrown from rust

```
>>> from xxx import sum_as_string
>>> sum_as_string(1,2)
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
TimeoutError: Task timed before computing finished
>>> sum_as_string(2,1)
'3'
```