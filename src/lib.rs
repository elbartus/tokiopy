// use tokio::prelude::*;
use tokio::time::{timeout, Duration, delay_for};
use pyo3::prelude::*;
use pyo3::wrap_pyfunction;
use pyo3::exceptions::PyTimeoutError;

async fn long_future(a: usize, b: usize) -> PyResult<String> {
    delay_for(Duration::from_secs(b as u64)).await;
    Ok((a + b).to_string())
}

/// Formats the sum of two numbers as string.
#[pyfunction]
fn sum_as_string(a: usize, b: usize) -> PyResult<String> {
    let mut rt = tokio::runtime::Runtime::new().unwrap();
    rt.block_on(async {
        timeout(Duration::from_secs(a as u64), long_future(a,b)).await
    }).unwrap_or(Err(PyErr::new::<PyTimeoutError, _>("Task timed before computing finished")))
}

/// A Python module implemented in Rust.
#[pymodule]
fn xxx(_py: Python, m: &PyModule) -> PyResult<()> {
    m.add_function(wrap_pyfunction!(sum_as_string, m)?)?;

    Ok(())
}